var dh = $(".wrapper").prop('scrollHeight'),
	wh = $(window).height(),
	scrollDis,
	toPer;

$(function(){
	scrollDis = $(".wrapper").scrollTop();
	activeSection(scrollDis);

});

$(window).resize(function(){
	var nH = $(window).height(),
	    dN = $(".wrapper").prop('scrollHeight');
	wh = nH;
	dh = dN;

	scrollDis = $(".wrapper").scrollTop();
	percentageBar(scrollDis);
});

$(".wrapper").scroll(function(){
	scrollDis = $(".wrapper").scrollTop();
	percentageBar(scrollDis);
	activeSection(scrollDis);
});




function percentageBar(scrollDis) {


	toPer = (scrollDis / (dh - wh) * 100).toFixed(0);
	$(".nav > h5 > .per").html(toPer);
	$(".nav").css("height", toPer+"%");
}

function activeSection() {
	$(".section").each(function(){


		if ((($(this).offset().top)) < 300 && (($(this).offset().top)) > (wh-(wh*2))+300){
			$(this).addClass("active");
		}
		else {
			$(this).removeClass("active");
		}
	});
}
